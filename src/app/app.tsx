import { ReactElement } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { Calc, NavBar, Home, Subscribe } from '../components/';

import './app.css';


type RoutingInformation = {
  name: string,
  component: ReactElement;
}

const routingInformation: Array<RoutingInformation> = [
  { name: 'home', component: <Home /> },
  { name: 'calc', component: <Calc /> },
  { name: 'subscribe', component: <Subscribe /> }
]

export function App(): ReactElement {
  return (
    <div className="app">
      <Router>
        <NavBar routes={routingInformation.map(({ name }) => name).filter((name) => name !== 'home')} />
        <Routes>
          {routingInformation.map(({ name, component }) => <Route path={`/${name}`} element={component} />)}
        </Routes>
      </Router>
    </div >
  );
}