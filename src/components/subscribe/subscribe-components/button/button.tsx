import { ReactElement, useEffect, useState } from "react";

import './button.css'

export function Button(): ReactElement {

    const [keyword, setKeyword] = useState('Subscribe')

    return <div className="subscribe-button" onClick={swap}>{keyword}</div >

    function swap(): void {
        if (keyword === 'Subscribe') {
            setKeyword("Unsubscribe")
        } else {
            setKeyword("Subscribe")
        }
    }

}