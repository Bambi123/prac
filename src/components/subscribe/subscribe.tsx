import { ReactElement } from "react";
import { Button } from "./subscribe-components";

export function Subscribe(): ReactElement {
    return <div id="subscribe">
        <Button />
    </div>
}