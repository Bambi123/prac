import { toNumber } from './to-number'

describe('toNumber', () => {
    it('should convert a string of 10 into a number', () => {
        expect(toNumber('10')).toEqual(10)
    })
})