import { processScatterData } from './process-scatter-data'

describe('processScatterData', () => {
    it('should process data appropriately', () => {
        expect(processScatterData([1, 2, 3])).toEqual([{ x: 1, y: 1, id: 1 }, { x: 2, y: 2, id: 2 }, { x: 3, y: 3, id: 3 }])
    })
})