import { ScatterValueType } from "@mui/x-charts"

export function processScatterData(pattern: Array<number>): ScatterValueType[] {
    let currId = 1;
    return pattern.map((value) => ({
        x: currId,
        y: value,
        id: currId++,
    }))
}