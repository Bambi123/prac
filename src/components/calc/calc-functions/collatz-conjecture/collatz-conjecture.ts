export function collatzConjecture(value: number): Array<number> {
    if (value === 1) return [1]
    if (value % 2 === 0) return [value, ...collatzConjecture(value / 2)]
    else return [value, ...collatzConjecture(3 * value + 1)]
}