import { collatzConjecture } from './collatz-conjecture'

describe('collatzConjecture', () => {
    it.each([
        [1, [1]],
        [2, [2, 1]],
        [10, [10, 5, 16, 8, 4, 2, 1]],
        [17, [17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]]
    ])('should build appropriate pattern for %s', (value, expectedResult) => {
        expect(collatzConjecture(value)).toEqual(expectedResult)
    })
})