import { ReactElement, useState } from "react"
import { FormControl, ScatterChart } from "./calc-components"
import { Subject } from 'rxjs';
import { collatzConjecture } from "./calc-functions";
import { CircularProgress, Divider } from "@mui/material";

export function Calc(): ReactElement {

    const renderSubject = new Subject<number>();
    const [loading, setLoading] = useState<boolean>(false)
    const [pattern, setPattern] = useState<Array<number>>([])

    renderSubject.subscribe(async (nextCollatz) => {
        setLoading(true)
        setPattern(collatzConjecture(nextCollatz));
        await sleep(1) // shhhh it's for user experience
        setLoading(false)
    })

    return (
        <div className="calc">
            <FormControl triggerCollatz={renderSubject.next.bind(renderSubject)} />
            <Divider style={{ backgroundColor: '#282c34', borderColor: '#282c34', display: 'hidden', margin: '20px' }} />
            {loading ? <>
                <CircularProgress style={{ marginTop: '100px' }} />
            </> : undefined}
            {!loading && pattern.length > 0 ? <ScatterChart pattern={pattern} /> : undefined}
        </div>
    )

    async function sleep(seconds: number): Promise<void> {
        return new Promise(res => {
            setTimeout(res, seconds * 1000)
        })
    }
}