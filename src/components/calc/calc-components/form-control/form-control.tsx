import { ReactElement, useState } from "react";
import { FormControl as MuiFormControl, InputLabel, Input, FormHelperText } from "@mui/material"
import { toNumber } from "../../calc-functions";

const ENTER_KEY_CODE = 13;

export interface IFormControl {
    triggerCollatz(value: number): void
}

export function FormControl({ triggerCollatz }: IFormControl): ReactElement {

    const [collatz, setCollatz] = useState<number>(-1)

    return (
        <MuiFormControl >
            <InputLabel style={{ color: 'white' }}>Enter a random number</InputLabel>
            <Input
                type="numeric"
                style={{ color: 'white' }}
                onChange={({ target: { value } }) => setCollatz(toNumber(value))}
                onKeyDown={(event) => {
                    if (event.keyCode === ENTER_KEY_CODE) {
                        triggerCollatz(collatz)
                    }
                }}
            />
            <FormHelperText style={{ color: 'white' }}>Let's use this information and do something cool</FormHelperText>
        </MuiFormControl>
    )
}