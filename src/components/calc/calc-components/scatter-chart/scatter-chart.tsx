import { ReactElement } from "react";
import { ScatterChart as MuiScatterChart } from '@mui/x-charts/ScatterChart';
import { processScatterData } from "../../calc-functions";

import './scatter-chart.css'

export interface IScatterChart {
    pattern: Array<number>
}

export function ScatterChart({ pattern }: IScatterChart): ReactElement {
    return (
        <div className='scatter-chart'>
            <MuiScatterChart
                width={600}
                height={300}
                series={[{
                    label: `Collatz Conjecture Plot for ${pattern[0]}`,
                    data: processScatterData(pattern),
                    markerSize: 5,
                    color: '#93DC5C',
                }]}
            />
        </div>

    )
}