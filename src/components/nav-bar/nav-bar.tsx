import { ReactElement } from "react";
import { AppBar, Toolbar, IconButton, Button } from "@mui/material";
import AddReactionIcon from '@mui/icons-material/AddReaction';
import { useNavigate } from "react-router-dom";

export interface INavBar {
    routes: Array<string>
}

export interface INavBarButton {
    name: string
}

export function NavBar({ routes }: INavBar): ReactElement {

    const navigate = useNavigate()

    return <div className='nav-bar'>
        <AppBar position="static" style={{ backgroundColor: '#00ff98', borderRadius: '40px', maxWidth: '250px', marginTop: '20px', marginLeft: '20px' }}>
            <Toolbar variant="dense">
                <IconButton edge="start" onClick={() => { console.log(':)'); navigate("/") }}>
                    <AddReactionIcon />
                </IconButton>
                {routes.map((route) => <NavBarButton name={route} />)}
            </Toolbar>
        </AppBar>
    </div >

    function NavBarButton({ name }: INavBarButton): ReactElement {
        return <Button style={{ color: '#282c34' }} onClick={() => navigate(`/${name}`)}>{name}</Button>
    }
}


